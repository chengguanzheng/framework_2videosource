//
// Created by Lenovo on 2021/6/30.
//

#include "DetectHandler.h"
#include <utility>
#include "opencv2/opencv.hpp"
#include "spdlog/spdlog.h"

using namespace std;
using namespace cv;

#define MODNAME "DetectHandler"
#define DEBUG(x) spdlog::debug("[{}] {}", MODNAME, x)
#define INFO(x) spdlog::info("[{}] {}", MODNAME, x)
#define LOG_ERROR(x) spdlog::error("[{}] {}", MODNAME, x)
#define CAMERA_NUM  2

DetectHandler::DetectHandler(std::string searchKey, std::string saveKey, HandlerType handlerType,
                             SignalType signalType,
                             std::vector<SignalType> listenSignalTypes):
        SignalHandler(handlerType, signalType,listenSignalTypes),
        searchKey(searchKey), saveKey(saveKey)
{}

void DetectHandler::doJobsLoop() {
    while(!mAbQuitDoJobsLoop){
        {
            std::unique_lock<std::mutex> lck(mMtxJobsBuffer);
            if(mJobsBuffer.empty()){
                mCvJobsBuffer.wait(lck);  //在signalHandler中的assignJobs函数中唤醒
            }
            if(!mJobsBuffer.empty()) job = mJobsBuffer.pop();
        }
        if(mAbQuitDoJobsLoop) break;

        Repository& repository = Repository::get();
        for(int i = 0; i < CAMERA_NUM; i++){
            if (!repository.hasKey(fmt::format(saveKey, i))) { //如果不包含saveKey类型的RIngCache，则创建
                repository.allocate(fmt::format(saveKey, i), 10, job.dataSize, true);
            }
            imageBuffer.resize(job.dataSize);
        }

        std::string key_search = job.camId == 0 ? fmt::format(searchKey, 0) : fmt::format(searchKey, 1);

        bool found = repository.getById(key_search, job.imageId, reinterpret_cast<void*>(imageBuffer.data()),
                                         job.dataSize);

        if (!found) {
            INFO(format("Image id {} of camera{} not found", job.imageId, job.camId));
            continue;
        }

        auto header = reinterpret_cast<FrameHeader*>(imageBuffer.data());
        header->timestamp = chrono::system_clock::now().time_since_epoch().count();

        auto img = Mat(header->height, header->width, CV_8UC(header->channel), imageBuffer.data() + sizeof(FrameHeader));
        GaussianBlur(img, img, Size(15, 15), 11, 11);//高斯滤波

        std::string key_save = job.camId == 0 ? fmt::format(saveKey, 0) : fmt::format(saveKey, 1);
        repository.push(key_save, imageBuffer.data(), imageBuffer.size());

        sendReadySignal(imageBuffer.size(), job.camId);
        DEBUG(format("Processed image {} of camera {}", job.imageId, job.camId));
        //INFO(format("get here"));
    }
}


long long DetectHandler::getImageId(){
    return (reinterpret_cast<FrameHeader*>(imageBuffer.data()))->id;
}
















