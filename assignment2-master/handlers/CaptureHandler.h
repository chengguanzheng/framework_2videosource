#pragma once

#include "framework/SignalHandler.h"
#include "utility/Repository.h"
#include "utility/CommonDef.h"


class CaptureHandler : public SignalHandler
{
public:
    explicit CaptureHandler(std::string key, int camId,
        HandlerType handlerType = HandlerType::CAPTURE,
        SignalType sendSignalType = SignalType::IMAGE_READY,
        std::vector<SignalType> listenSignalTypes = { SignalType::UNDEFINED });

    ~CaptureHandler() override;

    bool startCapture(const std::string &filepath);

    bool stopCapture();

private:

    long long idx;

    int camId;

    //std::shared_ptr<Repository> repository;

    std::string repoKey;

    void captureLoop();

    std::atomic_bool quitCaptureLoop{};

    std::thread captureThread;

    std::string filepath;

    void doJobsLoop() override;

    long long getImageId() override;

    std::vector<char> buffer;
};

