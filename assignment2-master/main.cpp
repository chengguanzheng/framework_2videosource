#include "ui/mainwindow.h"

#include <QApplication>

#include "utility/LoggerInit.h"

// To init logger
Logger logger;

int main(int argc, char *argv[])
{
    qRegisterMetaType<std::string>("std::string");

    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}
