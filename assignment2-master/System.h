#ifndef SYSTEM_H
#define SYSTEM_H

#include <handlers/CaptureHandler.h>
#include <handlers/DisplayHandler.h>
#include "handlers/DetectHandler.h"
#include "framework/Dispatcher.h"

class System {
public:
    System();

    ~System();

    bool start(const std::string &, int camId);

    bool stop();

    std::vector<std::shared_ptr<CaptureHandler>> captureHandlers;
    std::shared_ptr<DetectHandler> detectHandler;
    std::shared_ptr<DisplayHandler> displayHandler;
    //std::shared_ptr<Repository> repository;

private:
    bool started;
    //Dispatcher dispatcher;
    std::string filepath[2];

};

#endif // SYSTEM_H
