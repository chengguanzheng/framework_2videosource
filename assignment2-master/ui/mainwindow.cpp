#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QDebug>
#include "utility/Repository.h"

#include <spdlog/spdlog.h>
#include "opencv2/opencv.hpp"

#define MODNAME "MainWindow"
#define DEBUG(x) spdlog::debug("[{}] {}", MODNAME, x)
#define INFO(x) spdlog::info("[{}] {}", MODNAME, x)
#define LOG_ERROR(x) spdlog::error("[{}] {}", MODNAME, x)

using namespace std;
using namespace cv;

MainWindow::MainWindow(QWidget *parent)
        : QMainWindow(parent), ui(new Ui::MainWindow) {
    ui->setupUi(this);

    system = make_unique<System>();

    connect(ui->select, &QPushButton::clicked, this, [&]() {
        filepath = QFileDialog::getOpenFileName(this, tr("Please select a video file"), QString(), "*.mp4");
        INFO(fmt::format("Selected file {}", filepath.toStdString()));
        ui->file_path->setText(filepath);
    });

    connect(ui->select_2, &QPushButton::clicked, this, [&]() {
        filepath2 = QFileDialog::getOpenFileName(this, tr("Please select a video file"), QString(), "*.mp4");
        INFO(fmt::format("Selected file {}", filepath2.toStdString()));
        ui->file_path_2->setText(filepath2);
    });

    connect(ui->start, &QPushButton::clicked, this, [&]() {
        system->start(filepath.toStdString(), 0);
    });

    connect(ui->start_2, &QPushButton::clicked, this, [&]() {
        system->start(filepath2.toStdString(), 1);
    });

    connect(system->displayHandler.get(), &DisplayHandler::imageReady, this,
            [&](const string &key, long long imageId, int dataSize, int camId) {
                if (dataSize > buffer.size()) {
                    buffer.resize(dataSize);
                }

                auto found = Repository::get().getById(fmt::format(key, camId), imageId, buffer.data(), dataSize);
                if (!found) {
                    INFO(fmt::format("Image {} of cam {} not found in key {}", imageId, camId, key));
                    return;
                }
                auto header = (FrameHeader*)buffer.data();
                auto img = Mat(header->height, header->width, CV_8UC(header->channel), buffer.data() + sizeof(FrameHeader));
                cvtColor(img, img, COLOR_BGR2RGB);
                auto qimg = QImage((uchar*)buffer.data() + sizeof(FrameHeader),
                                   header->width, header->height, header->width * header->channel, QImage::Format_RGB888);
                if(camId == 0){
                    ui->image->setPixmap(QPixmap::fromImage(qimg).scaled(ui->image->width(), ui->image->height()));
                } else{
                    ui->image_2->setPixmap(QPixmap::fromImage(qimg).scaled(ui->image_2->width(), ui->image_2->height()));
                }
                //INFO(fmt::format("get here"));
            });
}

MainWindow::~MainWindow() {
    delete ui;
}

