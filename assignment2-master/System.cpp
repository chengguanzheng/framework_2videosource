#include "System.h"

#include <spdlog/spdlog.h>

#define MODNAME "System"
#define DEBUG(x) spdlog::debug("[{}] {}", MODNAME, x)
#define INFO(x) spdlog::info("[{}] {}", MODNAME, x)
#define LOG_ERROR(x) spdlog::error("[{}] {}", MODNAME, x)
#define CAMERA_NUMS 2
using namespace std;

System::System() : started(false) {
    //repository = make_shared<Repository>();
    captureHandlers.resize(CAMERA_NUMS);
    Dispatcher& dispatcher = Dispatcher::get();
    for(int i = 0;i<CAMERA_NUMS; i++){
        captureHandlers[i] = make_shared<CaptureHandler>(fmt::format(IMAGE_KEY_TEMPLATE, i), i);
        dispatcher.registerHandler(captureHandlers[i]);
    }

    detectHandler = make_shared<DetectHandler>( IMAGE_KEY_TEMPLATE, DETECT_KEY_TEMPLATE);
    displayHandler = make_shared<DisplayHandler>(DETECT_KEY_TEMPLATE);


    dispatcher.registerHandler(detectHandler);
    dispatcher.registerHandler(displayHandler);
    dispatcher.activateHandlers();


}

bool System::start(const std::string& filepath_, int camId) {
    if (filepath_.empty()) {
        LOG_ERROR("File path is empty!");
        return false;
    }

    filepath[camId] = filepath_;

    captureHandlers[camId]->startCapture(filepath[camId]);

    return true;
}

bool System::stop() {
    for(int i = 0; i++; i < CAMERA_NUMS)
        captureHandlers[i]->stopCapture();

    return false;
}

System::~System() {
    stop();
}