//
// Created by xf-huang on 2021/5/21.
//

#include <QStandardPaths>
#include "spdlog/spdlog.h"
#include "Config.h"

using std::make_unique;
using fmt::format;

#define MODNAME "Config"
#define DEBUG(x) spdlog::debug("[{}] {}", MODNAME, x)
#define INFO(x) spdlog::info("[{}] {}", MODNAME, x)
#define LOG_ERROR(x) spdlog::error("[{}] {}", MODNAME, x)

Config::Config() {
//    p = QStandardPaths::locate(QStandardPaths::AppConfigLocation, "config.ini");
    p = "./config.ini";
    INFO(format("Using config file from {}", p.toStdString()));
    if (p.isEmpty()) {
        LOG_ERROR("Configuration file NOT FOUND!");
        valid = false;
        return;
    }

    settings = make_unique<QSettings>(p, QSettings::Format::IniFormat);
}

int Config::getBufferLength() const {
    return settings->value("buffer/length").toInt();
}

QString Config::getSavePath() const {
    return settings->value("system/save_path").toString();
}

int Config::getCamCount() const {
    return settings->value("system/cam_count").toInt();
}

QString Config::getHost() const {
    return settings->value("db/host").toString();
}

int Config::getPort() const {
    return settings->value("db/port").toInt();
}

QString Config::getUsername() const {
    return settings->value("db/username").toString();
}

QString Config::getPassword() const {
    return settings->value("db/password").toString();
}

QString Config::getDbName() const {
    return settings->value("db/db_name").toString();
}



