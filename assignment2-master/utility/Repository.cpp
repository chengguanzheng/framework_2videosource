//
// Created by xf-huang on 12/22/20.
//

#include "Repository.h"
#include "IdBase.h"
#include "spdlog/spdlog.h"

using std::string;
using std::vector;
using ll = long long;

#define MODNAME "Repository"
#define DEBUG(x) spdlog::debug("[{}] {}", MODNAME, x)
#define INFO(x) spdlog::info("[{}] {}", MODNAME, x)
#define ERROR(x) spdlog::error("[{}] {}", MODNAME, x)

bool Repository::allocate(const string& key, int length, int eleSize, bool withId) {

    // No duplicate key
    if (mKeySet.count(key)) {
        ERROR(fmt::format("Cannot allocate memory with duplicated key \"{}\".", key));
        return false;
    }

    mVecMap.emplace(key, RingCache(length, eleSize));

    mKeySet.insert(key);
    if (withId) {

        mKeyWithId.insert(key);
    }
    return true;
}

bool Repository::free(const string& key) {
    if (!mKeySet.count(key)) {
        ERROR(fmt::format("Cannot free not existing key \"{}\".", key));
        return false;
    }

    mVecMap.erase(key);
    mKeySet.erase(key);
    if (mKeyWithId.count(key))
        mKeyWithId.erase(key);
    return true;
}

bool Repository::getById(const string& key, ll id, void* pVoid, int maxSize) const {
    if (!mKeySet.count(key)) {
        ERROR(fmt::format("Cannot getById from not existing key \"{}\".", key));
        return false;
    }

    if (!mKeyWithId.count(key)) {
        ERROR(fmt::format("Cannot getById from key \"{}\" without id.", key));
        return false;
    }

    if (maxSize < mVecMap.at(key).getElementSize()) {
        ERROR(fmt::format("Memory sz is not sufficient for data of key \"{}\", required sz is {}.", key,
            mVecMap.at(key).getElementSize()));
        return false;
    }
    if (mVecMap.at(key).empty()) {
        ERROR(fmt::format("Memory of key \"{}\" is empty.", key));
        return false;
    }

    auto eleSize = mVecMap.at(key).getElementSize();
    for (auto& vec : mVecMap.at(key).getDataRef()) {
        auto* p = (IdBase*)vec.data();
        if (p->id == id) {
            memcpy(pVoid, p, eleSize);
            return true;
        }
    }

    return false;
}

bool Repository::push(const string& key, void* pVoid, int size) {
    if (!mKeySet.count(key)) {
        ERROR(fmt::format("Cannot push to not existing key \"{}\".", key));
        return false;
    }

    mVecMap.at(key).push(pVoid, size);
    return true;
}

bool Repository::hasKey(const string &key) const {
    return mKeySet.count(key) == 1;
}
