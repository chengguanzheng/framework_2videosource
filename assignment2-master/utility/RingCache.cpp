//
// Created by xf-huang on 2020/12/24.
//

#include "RingCache.h"

#include <cassert>
#include <cstring>

using std::vector;

RingCache::RingCache(int cap, int sz)
    : sz(0)
    , front(0)
    , back(0)
    , capacity(cap)
    , eleSize(sz)
    , data(vector<vector<char>>(capacity, vector<char>(eleSize))) {}

void RingCache::push(void* pVoid, int size) {
    assert(size == eleSize);
    if (sz == capacity) {
        back = (back + 1) % capacity;
        memcpy(data[front].data(), pVoid, eleSize);
        front = (front + 1) % capacity;
    } else {
        memcpy(data[front].data(), pVoid, eleSize);
        front = (front + 1) % capacity;
        ++sz;
    }
}

void RingCache::pop(void* pVoid, int size) {
    assert(size >= eleSize);
    assert(sz > 0);
    memcpy(pVoid, data[back].data(), eleSize);
    back = (back + 1) % capacity;
    --sz;
}

int RingCache::size() const {
    return sz;
}

bool RingCache::empty() const {
    return sz == 0;
}

int RingCache::getElementSize() const {
    return eleSize;
}

const vector<vector<char>>& RingCache::getDataRef() const {
    return data;
}
